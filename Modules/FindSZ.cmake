#
# Find the SZ includes and library
#

# This module defines the following variables:
# SZ_INCLUDE_DIR, where to find sz.h
# SZ_LIBRARIES, the libraries to link against
# SZ_FOUND, if false, you cannot build anything that requires SZ

########################################################################
find_path(SZ_INCLUDE_DIR sz.h /usr/include /usr/local/include $ENV{SZ_DIR} $ENV{SZ_DIR}/include DOC "directory containing sz.h for SZ library")

set(SZ_NAMES sz zlib)
set(SZ_LIBRARIES)
foreach(name ${SZ_NAMES})
	unset(SZ_LIB CACHE)
	find_library(SZ_LIB NAMES ${name} PATHS /usr/lib /usr/local/lib $ENV{SZ_DIR}/lib $ENV{SZ_DIR} DOC "SZ library file")
	list(APPEND SZ_LIBRARIES ${SZ_LIB})
endforeach()

if (SZ_INCLUDE_DIR AND SZ_LIBRARIES)
	set(SZ_FOUND TRUE)
else ()
	set(SZ_FOUND FALSE)
endif ()

if (SZ_FOUND)
	if (NOT SZ_FIND_QUIETLY)
		message(STATUS "Found SZ library: ${SZ_LIBRARIES}")
		message(STATUS "Found SZ include: ${SZ_INCLUDE_DIR}")
	endif ()
else ()
	if (SZ_FIND_REQUIRED)
		message(FATAL_ERROR "Could not find SZ")
	endif ()
endif ()
